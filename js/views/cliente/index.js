var vm = new Vue({
  el: '.content',
  data: {
    clientes: [],
    clienteEnEdicion: undefined,
    clienteCopia: undefined
  },
  methods:{
    modifica: function(cliente){
      if(cliente===this.clienteEnEdicion){ //graba
        this.clienteEnEdicion = undefined;
        this.clienteCopia = undefined;
      }else{
        this.clienteEnEdicion = cliente;
        this.clienteCopia = Object.assign({},cliente);
        setTimeout(function(){
            $('#id_'+cliente.id).focus();
        })
      }
    },
    cancelaBorra: function(cliente){
      if(cliente===this.clienteEnEdicion){ //cancela
        var pos = _.findIndex(this.clientes, { 'id':cliente.id });
        Object.assign(this.clientes[pos], this.clienteCopia);
        this.clienteEnEdicion = undefined;
        this.clienteCopia = undefined;
      }else{ //borra
        alert('borra');
      }
    }
  }
})
