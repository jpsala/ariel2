<html xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:website="http://ogp.me/ns/website" lang="en-US" itemscope="" itemtype="http://schema.org/WebPage" class="yui3-js-enabled js flexbox canvas canvastext webgl no-touch hashchange history draganddrop rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms no-csstransforms3d csstransitions video audio svg inlinesvg svgclippaths wf-proximanova-i3-active wf-proximanova-i6-active wf-proximanova-i7-active wf-proximanova-n3-active wf-proximanova-n4-active wf-proximanova-n6-active wf-proximanova-n7-active wf-active"
    style="">
<div id="yui3-css-stamp" style="position: absolute !important; visibility: hidden !important"></div>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="viewport" content="initial-scale=1">

    <base href="">
    <meta charset="utf-8">
    <title>Juan Pablo Sala</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <link rel="canonical" href="http://www.callieschweitzer.com/">
    <meta property="og:site_name" content="Juan Pablo Sala">
    <meta property="og:title" content="Home">
    <meta property="og:url" content="http://www.jpsala.com.ar/">
    <meta property="og:type" content="website">
    <meta itemprop="name" content="Home">
    <meta itemprop="url" content="http://www.jpsala.com.ar/">
    <meta name="description" content="">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Varela+Round:400,normal,400italic,700italic,normalitalic,700normal">
    <link rel="stylesheet" type="text/css" href="//static1.squarespace.com/static/sitecss/531499abe4b03cbb51137883/2/50521cf884aeb45fa5cfdb80/531499abe4b03cbb51137887/879-05142015/1428695895810/site.css?&amp;filterFeatures=false">
    <style type="text/css">
        body,
        p,
        nav li,
        li {
            font-size: 18px !important;
        }

        body,
        p {
            color: hsl(0, 0%, 27%) !important;
        }

        ul {
            line-height: 1.7 !important;
        }

        h1.logo {
            font-size: 26px !important;
        }

        .site-info {
            display: none !important;
        }

        .sqs-lightbox-meta> a:link,
        a:visited {
            color: #fff;
        }
    </style>
</head>

<body class="page-borders-thick canvas-style-normal  header-subtitle-address banner-alignment-center blog-layout-center project-layout-left-sidebar thumbnails-on-open-page-show-all social-icon-style-round    hide-page-title   hide-article-author product-list-titles-under product-list-alignment-center product-item-size-32-standard product-image-auto-crop product-gallery-size-11-square product-gallery-auto-crop show-product-price show-product-item-nav product-social-sharing   event-thumbnails event-thumbnail-size-32-standard event-date-label event-date-label-time event-list-show-cats event-list-date event-list-time event-list-address   event-icalgcal-links  event-excerpts  event-item-back-link      opentable-style-light newsletter-style-dark small-button-style-solid small-button-shape-square medium-button-style-solid medium-button-shape-square large-button-style-solid large-button-shape-square button-style-outline button-corner-style-square tweak-product-quick-view-button-style-floating tweak-product-quick-view-button-position-bottom tweak-product-quick-view-lightbox-excerpt-display-truncate tweak-product-quick-view-lightbox-show-arrows tweak-product-quick-view-lightbox-show-close-button tweak-product-quick-view-lightbox-controls-weight-light native-currency-code-usd collection-5324b571e4b05d171ab8ded8 collection-type-page collection-layout-default homepage mobile-style-available site-title"
    id="collection-5324b571e4b05d171ab8ded8" cz-shortcut-listen="true">

    <div id="canvas">

        <div id="mobileNav" class="">
            <div class="wrapper">
                <nav class="main-nav mobileNav">
                    <ul>
                        <li class="page-collection active-link">
                            <a href="/">Home</a>
                        </li>
                        <li class="page-collection">
                            <a href="/media-commentary/">Media Commentary</a>
                        </li>
                        <li class="page-collection">
                            <a href="/contact/">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div id="mobileMenuLink"><a>Menu</a></div>
        <header id="header" class="clear">
            <div id="upper-logo">
                <h1 class="logo" data-content-field="site-title">
          <a href="/">Juan Pablo Sala</a>
            </div>
            <script>
                if (parseInt(Y.one('body').getComputedStyle('width'), 10) <= 640) {
                    Y.use('squarespace-ui-base', function(Y) {
                        Y.one("#upper-logo .logo").plug(Y.Squarespace.TextShrink, {
                            parentEl: Y.one('#upper-logo')
                        });
                    });
                }
            </script>


            <div class="site-info">
                <div class="site-address">Street Address</div>
                <div class="site-city-state">New York, NY</div>
                <div class="site-phone">Phone Number</div>
            </div>


            <div class="site-tag-line">
                <span>Defining digital-age journalism with honesty, integrity and transparency</span>
            </div>

            <div class="custom-info">
                <div class="sqs-layout sqs-grid-12 columns-12" data-layout-label="Header Subtitle: Custom Content" data-type="block-field" data-updated-on="1381761023501" id="customInfoBlock">
                    <div class="row sqs-row">
                        <div class="col sqs-col-12 span-12">
                            <div class="sqs-block html-block sqs-block-html" data-block-type="2" id="block-81eee4bbda3d9efd0834">
                                <div class="sqs-block-content">
                                    <p class="text-align-center">Your Custom Text Here</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="lower-logo">
                <h1 class="logo" data-content-field="site-title" data-shrink-original-size="26" style="letter-spacing: 0.115385em;"><a href="/">Callie Schweitzer</a></h1>
            </div>
            <script>
                Y.use('squarespace-ui-base', function(Y) {
                    Y.one("#lower-logo .logo").plug(Y.Squarespace.TextShrink, {
                        parentEl: Y.one('#lower-logo')
                    });
                });
            </script>


            <div id="topNav">
                <nav class="main-nav" data-content-field="navigation">
                    <ul>


                        <!-- <li class="page-collection active-link">




              <a href="/">Home</a>







        </li> -->



                        <li class="page-collection">
                            <a href="/contact/">Gestión educativa</a>
                        </li>

                        <li class="page-collection">
                            <a href="/contact/">Desarrolos a medida</a>
                        </li>

                        <li class="page-collection">




                            <a href="/contact/">Contacto</a>







                        </li>


                    </ul>
                    <div class="page-divider"></div>
                </nav>
            </div>


        </header>

        <div class="page-divider top-divider"></div>

        <!-- // page image or divider -->




        <section id="page" class="clear" role="main" data-content-field="main-content" data-collection-id="5324b571e4b05d171ab8ded8" data-edit-main-image="Banner" style="opacity: 1;">

            <!-- // CATEGORY NAV -->


            <div class="sqs-layout sqs-grid-12 columns-12" data-type="page" data-updated-on="1457797723703" id="page-5324b571e4b05d171ab8ded8">
                <div class="row sqs-row">
                    <div class="col sqs-col-12 span-12">
                        <div class="sqs-block code-block sqs-block-code" data-block-type="23" id="block-yui_3_10_1_1_1398735470978_5603">
                            <div class="sqs-block-content"><img src="../images/uno.jpg" style="float:right; width: 280px; padding: 0 0 1em 2em;">
                                <p>
                                    Mi nombre es Juan Pablo Sala, soy desarrollador desde los 17 años, mis pasiones son mis tres hijos, mi esposa y programar.
                                </p>
                                <p>
                                    Desde el año 2000 me especializo en software para gestianar colegios. Me gusta estar al día en cuanto a tecnología para poder usar las últimas herramientas disponibles.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Home Footer Content" data-type="block-field" data-updated-on="1394917278465" id="collection-5324b571e4b05d171ab8ded8">
            <div class="row sqs-row">
                <div class="col sqs-col-12 span-12"></div>
            </div>
        </div>

        <!-- <div class="page-divider bottom-divider"></div> -->

        <div class="info-footer-wrapper clear">
            <div class="info-footer">
                <div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Info Footer Content" data-type="block-field" data-updated-on="1395005522954" id="infoFooterBlock">
                    <div class="row sqs-row">
                        <div class="col sqs-col-12 span-12"></div>
                    </div>
                </div>


                <div id="socialLinks" class="social-links sqs-svg-icon--list" data-content-field="connected-accounts">

                    <a href="https://twitter.com/cschweitz" target="_blank" class="sqs-svg-icon--wrapper twitter" style="">
                        <div>
                            <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--background" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#background"></use>
                                <use class="sqs-use--icon" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#twitter-icon"></use>
                                <use class="sqs-use--mask" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#twitter-mask"></use>
                            </svg>
                        </div>
                    </a>

                    <a href="http://www.facebook.com/1365480210" target="_blank" class="sqs-svg-icon--wrapper facebook" style="">
                        <div>
                            <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--background" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#background"></use>
                                <use class="sqs-use--icon" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#facebook-icon"></use>
                                <use class="sqs-use--mask" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#facebook-mask"></use>
                            </svg>
                        </div>
                    </a>

                    <a href="http://www.linkedin.com/in/callieschweitzer" target="_blank" class="sqs-svg-icon--wrapper linkedin" style="">
                        <div>
                            <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--background" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#background"></use>
                                <use class="sqs-use--icon" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#linkedin-icon"></use>
                                <use class="sqs-use--mask" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#linkedin-mask"></use>
                            </svg>
                        </div>
                    </a>

                    <a href="http://instagram.com/cschweitz" target="_blank" class="sqs-svg-icon--wrapper instagram" style="">
                        <div>
                            <svg class="sqs-svg-icon--social" viewBox="0 0 64 64">
                                <use class="sqs-use--background" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#background"></use>
                                <use class="sqs-use--icon" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#instagram-icon"></use>
                                <use class="sqs-use--mask" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/universal/svg/social-accounts.svg#instagram-mask"></use>
                            </svg>
                        </div>
                    </a>

                </div>


            </div>
        </div>

        <footer id="footer" class="clear">
            <div class="sqs-layout sqs-grid-12 columns-12 empty" data-layout-label="Footer Content" data-type="block-field" data-updated-on="1395005522986" id="footerBlock">
                <div class="row sqs-row">
                    <div class="col sqs-col-12 span-12"></div>
                </div>
            </div>
        </footer>

    </div>

    <div></div>

    <script type="text/javascript" data-sqs-type="imageloader-bootstraper">
        (function() {
            if (window.ImageLoader) {
                window.ImageLoader.bootstrap();
            }
        })();
    </script>
    <script>
        Squarespace.afterBodyLoad(Y);
    </script>






</body>
<div id="cVim-status-bar" style="top: 0px;"></div>
<iframe src="chrome-extension://ihlenndgcmojhcghmfjfneahoeklbjjh/cmdline_frame.html" id="cVim-command-frame"></iframe>

</html>
