<div class="ui four responsive cards">
  <div class="card" v-for="cliente in clientes">
    <div class="content">
      <div class="header">{{cliente.nombre}}</div>
      <div class="meta">{{cliente.observaciones}}</div>
      <div class="description">
        <i class="Mail Outline icon"></i>{{cliente.correo}}
      </div>
      <div class="description">
        <i class="Phone Outline icon"></i>{{cliente.telefonos}}
      </div>
    </div>
  </div>
</div>
<script src="../js/views/cliente/index.js"></script>
<script type="text/javascript">
    vm.clientes = <?php echo json_encode($data); ?> || [];
</script>
