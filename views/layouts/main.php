<!-- <script type="text/javascript" src="../vendor/bower/knockout/dist/knockout.js"></script> -->
<script src="../js/lodash.min.js"></script>
<script type="text/javascript" src="../js/vue.min.js"></script>
<script type="text/javascript" src="../js/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="../js/moment.min.js"></script>
<script type="text/javascript" src="../js/moment.locale.es.js"></script>
<script type="text/javascript">moment.locale('sp');</script>
<link rel="stylesheet" type="text/css" href="../semantic/dist/semantic.min.css">
<script src="../semantic/dist/semantic.min.js"></script>
<?php $action=$this->context->action->controller->id . $this->context->action->id;?>
<body>
  <div class="ui pointing menu">
    <a class="{{currentAction==='siteindex'?'active':''}} item" href="?r=/site">
      Trabajos
    </a>
    <a class="{{currentAction==='clienteindex'?'active':''}} item" href="?r=/cliente">
      Clientes
    </a>
    <a class="{{currentAction==='sitelogin'?'active':''}} item" href="?r=/site/login">
      Friends
    </a>
    <div class="right menu">
      <div class="item">
        <div class="ui transparent icon input">
          <input type="text" placeholder="Search...">
          <i class="search link icon"></i>
        </div>
      </div>
    </div>
  </div>
  <div class="ui {{loading?'loading':''}} segment content">
    <?= $content ?>
  </div>
</body>

<script src="../js/main.js"></script>
<script type="text/javascript">
    vmNav.currentAction = "<?php echo $action ;?>";
</script>
